class Bob:
  def __init__(self):
    self.name = "Bob"

  def hey(whatsup):
    if whatsup == '':
      print 'Fine. Be that way.'
    elif whatsup.endswith('?'):
      print 'Sure.'
    elif whatsup.isupper():
      print 'Woah, chill out!'
    else:
      print 'Whatever.'


